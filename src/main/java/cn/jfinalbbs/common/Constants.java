package cn.jfinalbbs.common;

/**
 * Created by liuyang on 15/4/2.
 */
public class Constants {

    public static final String ERROR = "error";
    public static final String USER_COOKIE = "user";
    public static final String USER_SESSION = "user";
    public static final String BEFORE_URL = "before_url";
    public static final String ADMIN_BEFORE_URL = "admin_before_url";
    public static final String ADMIN_USER_SESSION = "admin_user";
    public static final String TODAY = "today";
    public static final String NOTIFICATION_MESSAGE1 = "回复了你的话题";
    public static final String NOTIFICATION_MESSAGE2 = "引用了你的回复";

    public static final String OP_ERROR_MESSAGE = "非法操作";

    public static final String COOKIE_USERNAME = "username";

    public static class ResultCode {
        public static final String SUCCESS = "200";
        public static final String FAILURE = "201";
    }

    public static class ResultDesc {
        public static final String SUCCESS = "success";
        public static final String FAILURE = "error";
    }

}
